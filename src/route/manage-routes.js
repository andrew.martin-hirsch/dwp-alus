const express = require('express');
const {getRegisterScreen} = require("../controller/get-register-screen");
const {getLoginScreen} = require("../controller/get-login-screen");
const {getAddSuggestionScreen} = require("../controller/get-add-suggestion-screen");
const {getEditSuggestionScreen} = require("../controller/get-edit-suggestion-screen");
const {getAcronyms} = require('../sqlite/database');
const {getSuggestions, postSuggestion, deleteSuggestion, addSuggestion, editSuggestion} = require('../sqlite/suggestion-handling');
const {postRegisterRequest} = require("../sqlite/register-user-handling");
const {postLoginRequest} = require("../sqlite/login-handling");
const {logout} = require("../controller/logout");
const {setCSRFToken, compareCSRFToken} = require("../middleware/csrf");

const getRoutes = () => {
    const app = express.Router();
    app.get('/acronyms', setCSRFToken, getAcronyms);
    app.get('/home', setCSRFToken, getAcronyms);
    app.get('/add-suggestion', setCSRFToken, getAddSuggestionScreen);
    app.get('/register', setCSRFToken, getRegisterScreen);
    app.get('/login', setCSRFToken, getLoginScreen);
    app.get('/view-suggestions', setCSRFToken, getSuggestions);
    app.post('/api/register', compareCSRFToken, postRegisterRequest);
    app.post('/api/login', compareCSRFToken, postLoginRequest);
    app.post('/api/suggestion', compareCSRFToken, postSuggestion);
    app.post('/add-suggestion', compareCSRFToken, addSuggestion);
    app.post('/delete-suggestion', compareCSRFToken, deleteSuggestion);
    app.post('/edit-suggestion', setCSRFToken, getEditSuggestionScreen);
    app.post('/submit-edit', compareCSRFToken, editSuggestion);
    app.post('/logout', logout);
    return app;
};

module.exports = {
    getRoutes,
};
