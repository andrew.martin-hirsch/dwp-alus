const { v4: uuidv4 } = require('uuid');
const CSRFAttackError = require('./CSRFAttackError');

const setCSRFToken = (req, _res, next) => {
  req.session.csrf = uuidv4();
  next();
};

const compareCSRFToken = (req, _res, next) => {
  if (req.session.csrfChecked) {
    delete req.session.csrfChecked;
    next();
  } else {
    const csrfAct = req.session.csrf;
    const { csrf } = req.body;
    if (!csrfAct || csrfAct !== csrf) {
      throw new CSRFAttackError();
    }
    delete req.session.csrf;
    next();
  }
};

module.exports = {
  setCSRFToken,
  compareCSRFToken,
};
