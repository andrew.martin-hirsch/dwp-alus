class CSRFAttackError extends Error {
  constructor() {
    super('Detected a Cross-Site Request Forgery Attempt');
    this.name = 'CSRFAttackError';
  }
}

module.exports = CSRFAttackError;
