const logger = require("./utils/logger");

const getEditSuggestionScreen = async (req, res) => {
    try {
        logger.info('Rendering edit suggestion page');
        const data = {
            csrf: req.session.csrf,
            suggestion_id : req.body.suggestion_id,
        };
        res.render('edit-suggestion', data);
    } catch (err) {
        logger.error({ message: 'Error occurred rendering Edit Suggestion page', reason: err.message });
        res.render('error-page');
    }
};

module.exports = {
    getEditSuggestionScreen,
};
