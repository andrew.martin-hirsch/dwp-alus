const logger = require("./utils/logger");

const getLoginScreen = async (req, res) => {
    try {
        logger.info('Rendering login page');
        const data = {
            csrf: req.session.csrf,
            err: req.session.err,
        };
        delete req.session.err;
        res.render('login', data);
    } catch (err) {
        logger.error({ message: 'Error occurred rendering login page', reason: err.message });
        res.render('error-page');
    }
};

module.exports = {
    getLoginScreen,
};
