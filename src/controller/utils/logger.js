const dwpNodeLogger = require('@dwp/node-logger');

const logger = dwpNodeLogger('api', { logLevel: 'debug' });

module.exports = logger;
