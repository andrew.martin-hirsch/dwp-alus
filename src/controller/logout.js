const {getAcronyms} = require("../sqlite/database");
const logger = require("./utils/logger");

const logout = (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            logger.error({ message: 'Error occurred when attempting to destroy session', reason: err.message });
            res.render('error-page');
        } else {
            logger.info('User logged out, session is destroyed');
            getAcronyms(req, res);
        }
    });
};

module.exports = {
    logout,
};
