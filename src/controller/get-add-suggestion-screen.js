const logger = require("./utils/logger");

const getAddSuggestionScreen = async (req, res) => {
    try {
        logger.info('Rendering add suggestion page');
        const data = {
            csrf: req.session.csrf,
        };
        res.render('add-suggestion', data);
    } catch (err) {
        logger.error({ message: 'Error occurred rendering Add Suggestion page', reason: err.message });
        res.render('error-page');
    }
};

module.exports = {
    getAddSuggestionScreen,
};
