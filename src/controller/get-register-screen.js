const logger = require("./utils/logger");

const getRegisterScreen = async (req, res) => {
    try {
        logger.info('Rendering register page');
        const data = {
            csrf: req.session.csrf,
            user_exists_error: req.session.err,
            password_match_error: req.session.passworderror
        };
        delete req.session.err;
        delete req.session.passworderror;
        res.render('register', data);
    } catch (err) {
        logger.error({ message: 'Error occurred rendering register page', reason: err.message });
        res.render('error-page');
    }
};

module.exports = {
    getRegisterScreen,
};
