const logger = require("./utils/logger");

const getHome = async (req, res) => {
    try {
        logger.info('Rendering home page');
        res.render('home');
    } catch (err) {
        logger.error({ message: 'Error occurred rendering home page', reason: err.message });
        res.render('error-page');
    }
};
module.exports = {
    getHome,
};
