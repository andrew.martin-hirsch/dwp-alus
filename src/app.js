const express = require('express');
const path = require('path');
const app = express();
const session = require('express-session');
require("dotenv").config();
const cors = require('cors');
const helmet = require('helmet');
const manageRoutes = require('./route/manage-routes').getRoutes();
const redirectRoutes = require('./route/redirect-routes').routeRedirect();
const {createDatabase} = require('./sqlite/database');
const { nunjucksConfig } = require('./nunjucks-config');
const { returnConfig } = require("./config");
const logger = require("./controller/utils/logger");

const appConfig = returnConfig(process.env);

const port = appConfig.PORT || 3004;

nunjucksConfig(app);

createDatabase();

app.use(helmet({
    contentSecurityPolicy: {
        directives: {
            'default-src': ["'none'"],
        },
    },
    strictTransportSecurity: {
        maxAge: appConfig.MAX_AGE,
    },
}));

app.use(session({
    secret: 'mysecretkey', // Change this
    resave: false,
    saveUninitialized: true
}));

app.use(
    express.urlencoded(),
    cors({
        origin: 'http://localhost:3000'
    })
);

app.use('/assets', express.static(path.resolve(__dirname, '../assets'), ));

app.use('/', manageRoutes);
app.use('/', redirectRoutes);

app.listen(port, () => logger.info(`API listening on port ${port}!`));
