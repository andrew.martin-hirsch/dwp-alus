function applyDefaultSettings(settings) {
    const defaultSettings = {
        PORT: 3004,
        MAX_AGE: 63072000,
    };
    return {
        ...defaultSettings,
        ...settings,
    };
}

function returnConfig(env) {
    const config = applyDefaultSettings(env);
    return {
        ...config,
    };
}

module.exports = {
    returnConfig,
    applyDefaultSettings,
};
