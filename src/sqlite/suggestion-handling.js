const logger = require("../controller/utils/logger");
const joi = require("joi");
const sqlite3 = require('sqlite3').verbose()
const databaseSource = "alus.db"
require("dotenv").config();

const validatePostBody = (body) => {
    const schema = joi.object({
        acronym: joi.string().required(),
        definition: joi.string().required(),
        description: joi.string().required(),
        csrf: joi.string(),
        suggestion_id: joi.string(),
    });
    return schema.validate(body, {
        abortEarly: false,
    });
};

// A function that handles a post request to store a new acronym suggestion
const postSuggestion = async (req, res) => {
    const user_id = req.session.userId;
    const { acronym, definition, description } = req.body;

    const validateFilter = validatePostBody(req.body);
    if (validateFilter.error) {
        const { message } = validateFilter.error;
        logger.info(`Invalid post body ${message}`);
        return res.status(400).send(message);
    }

    if (!user_id) {
        res.status(400).send('Missing user id, please log in');
        return;
    }

    const alusDatabase = new sqlite3.Database(databaseSource);

    alusDatabase.run(
        'INSERT INTO suggestions (acronym, definition, description, user_id) VALUES (?, ?, ?, ?)',
        [acronym, definition, description, user_id],
        (err) => {
            if (err) {
                console.error(err.message);
                res.status(500).send('Error adding suggestion to database');
                return;
            }

            return res.render('suggestion_success.njk');
        }
    );

    alusDatabase.close();
};

// A function that retrieves all the user suggestions and sends them to the suggestions page
const getSuggestions = (req, res) => {
    const alusDatabase = new sqlite3.Database(databaseSource);

    // Query the suggestions table to retrieve all suggestions
    alusDatabase.all('SELECT * FROM Suggestions', (err, rows) => {
        if (err) {
            logger.error({ message: 'Error occurred when attempting to collect suggestions', reason: err.message });
            res.status(500).send('Error retrieving suggestions from database');
            return;
        }

        // Render the view-suggestions.njk page and pass the suggestions as context
        res.render('view-suggestions', { username: req.session.username, is_admin: req.session.isAdmin, suggestions: rows, csrf: req.session.csrf });
    });

    alusDatabase.close();
};

// A function that deletes a user suggestion by suggestion id
const deleteSuggestion = (req, res) => {
    const suggestion_id = req.body.suggestion_id;
    const alusDatabase = new sqlite3.Database(databaseSource);

    alusDatabase.run(`DELETE FROM Suggestions WHERE suggestion_id = ?`, suggestion_id, (err) => {
        if (err) {
            logger.error({ message: 'Error occurred when attempting to delete user', reason: err.message });
            res.status(500).send('Error deleting suggestion from database');
        } else {
            logger.info('Suggestion deleted');
            return res.redirect("/view-suggestions");
        }
    });

    alusDatabase.close();
};

const addSuggestion = (req, res) => {
    // Get the suggestion ID from the request
    const suggestion_id = req.body.suggestion_id;

    // Creating a new database connection
    const alusDatabase = new sqlite3.Database(databaseSource);

    // Update the suggestion
    alusDatabase.run(`UPDATE Suggestions SET is_approved = 1 WHERE suggestion_id = ?`, suggestion_id, function (err) {
        if (err) {
            logger.error({ message: 'Error occurred when attempting to login', reason: err.message });
            res.status(500).send('Error updating suggestion in database');
            return;
        }

        // Add the suggestion to the acronyms table
        const insertStatement = `INSERT INTO Acronyms (acronym, definition, description) 
                              SELECT acronym, definition, description FROM Suggestions 
                              WHERE suggestion_id = ?`;
        alusDatabase.run(insertStatement, suggestion_id, function (err) {
            if (err) {
                logger.error({ message: 'Error occurred when attempting to add suggestion to ALUS DB', reason: err.message });
                res.status(500).send('Error adding suggestion to database');
                return;
            }
            alusDatabase.run(
                'DELETE FROM Suggestions WHERE suggestion_id = ?',
                [suggestion_id],
                (err) => {
                    if (err) {
                        logger.error({ message: 'Error occurred when attempting to delete suggestion from suggestion table', reason: err.message });
                        res.status(500).send('Error deleting suggestion from database');
                        return;
                    }

                    logger.info(`Suggestion with ID ${suggestion_id} added to acronyms`);

                    alusDatabase.close();

                    // Reload suggestions and direct to the view suggestions page
                    return res.redirect("/view-suggestions");
                });
        });
    });
};

const editSuggestion = (req, res) => {
    const validateFilter = validatePostBody(req.body);
    if (validateFilter.error) {
        const { message } = validateFilter.error;
        logger.info(`Invalid post body ${message}`);
        return res.status(400).send(message);
    }
    const suggestion_id = req.body.suggestion_id;
    const newAcronym = req.body.acronym;
    const newDefinition = req.body.definition;
    const newDescription = req.body.description;

    const alusDatabase = new sqlite3.Database(databaseSource);
    alusDatabase.run(
        `UPDATE Suggestions SET acronym=?, definition=?, description=? WHERE suggestion_id=?`,
        [newAcronym, newDefinition, newDescription, suggestion_id],
        (err) => {
            if (err) {
                logger.error({ message: 'Error occurred when edit suggestion', reason: err.message });
                res.status(500).send('Error updating suggestion');
                return;
            }
            logger.info(`Suggestion ${suggestion_id} updated`);
            alusDatabase.close();
            return res.redirect("/view-suggestions");
        }
    );
};

module.exports = {
    getSuggestions,
    postSuggestion,
    deleteSuggestion,
    addSuggestion,
    editSuggestion,
};
