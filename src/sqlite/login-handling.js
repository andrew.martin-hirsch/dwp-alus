const bcrypt = require("bcryptjs");
const logger = require("../controller/utils/logger");
const joi = require('joi');
const {getAcronyms} = require("./database");
const sqlite3 = require('sqlite3').verbose()
const databaseSource = "alus.db"
require("dotenv").config();

const validatePostBody = (body) => {
    const schema = joi.object({
        email: joi.string().email().required(),
        password: joi.string().required(),
        csrf: joi.string(),
    });
    return schema.validate(body, {
        abortEarly: false,
    });
};

// A function that handles a post request to log an existing user into their account
const postLoginRequest = async (req, res) => {
    const alusDatabase = new sqlite3.Database(databaseSource, async (err) => {
        if (err) {
            // Cannot open database
            logger.error({ message: 'Error occurred when attempting to connect to ALUS DB', reason: err.message });
            throw err
        } else {
            logger.info('Connection to ALUS DB was successful');
        }
        try {
            const validateFilter = validatePostBody(req.body);
            if (validateFilter.error) {
                const { message } = validateFilter.error;
                logger.info(`Invalid post body ${message}`);
                return res.status(400).send(message);
            }

            const {email, password} = req.body;

            let user = [];

            const sql = "SELECT * FROM Users WHERE email = ?";

            alusDatabase.all(sql, email, async function (err, rows) {
                if (err) {
                    res.status(400).json({"error": err.message})
                    return;
                }

                rows.forEach(function (row) {
                    user.push(row);
                })

                if (typeof user === "undefined" || typeof user[0] === "undefined") {
                    req.session.err = "User does not exist";
                    return res.redirect("/login");
                }

                // Compare password with hash using bcrypt
                const passwordMatch = await bcrypt.compare(password, user[0].password);

                // If password doesn't match then render login page with error message
                if (!passwordMatch) {
                    req.session.err = "Invalid password";
                    return res.redirect("/login");
                }

                //Set session variables
                req.session.userId = user[0].user_id;
                req.session.username = user[0].username;
                req.session.isAdmin = user[0].is_admin;
                const username = user[0].username;
                //Reload home page with acronyms
                getAcronyms(req, res);
            });
        } catch (err) {
            logger.error({ message: 'Error occurred when attempting to login', reason: err.message });
            res.redirect("/login");
        }
    });
    alusDatabase.close();
}

module.exports = {
    postLoginRequest,
};
