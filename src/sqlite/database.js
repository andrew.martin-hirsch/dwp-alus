const bcrypt = require("bcryptjs");
const logger = require("../controller/utils/logger");
const sqlite3 = require('sqlite3').verbose()
const databaseSource = "alus.db"
require("dotenv").config();

// A function that creates a database if one doesn't exist
const createDatabase = () => {
    const alusDatabase = new sqlite3.Database(databaseSource, (err) => {
        if (err) {
            // Cannot open database
            logger.error({ message: 'Error occurred when attempting to connect to ALUS DB', reason: err.message });
            res.render('error-page');
        }
        else {

            const salt = bcrypt.genSaltSync(10);

            logger.info('Building user table');
            alusDatabase.run(`CREATE TABLE Users ( 
                            user_id INTEGER PRIMARY KEY AUTOINCREMENT,
                            username TEXT NOT NULL,
                            email TEXT NOT NULL,
                            password TEXT NOT NULL,
                            salt TEXT,
                            token TEXT,
                            is_admin BOOLEAN NOT NULL DEFAULT 0,
                            date_created DATE )`,

                (err) => {
                    if (err) {
                        logger.info('User table already exists');
                        // Table already created
                    } else {
                        logger.info('Adding default user list to user table');
                        // Table created, creating some rows
                        const insert = 'INSERT INTO Users (username, email, password, salt, date_created, is_admin) VALUES (?,?,?,?,?,?)'
                        alusDatabase.run(insert, ["user1", "user1@example.com", bcrypt.hashSync("user1", salt), salt, Date('now'), 0])
                        alusDatabase.run(insert, ["user2", "user2@example.com", bcrypt.hashSync("user2", salt), salt, Date('now'), 0])
                        alusDatabase.run(insert, ["admin", "admin@example.com", bcrypt.hashSync("admin", salt), salt, Date('now'), 1])
                    }
                });
            logger.info('Building suggestions table');
            alusDatabase.run(`CREATE TABLE Suggestions (
                            suggestion_id INTEGER PRIMARY KEY AUTOINCREMENT,
                            acronym TEXT NOT NULL, definition TEXT NOT NULL,
                            description TEXT NOT NULL, user_id INTEGER NOT NULL,
                            is_approved INTEGER DEFAULT 0,
                            FOREIGN KEY (user_id) REFERENCES Users (user_id) )`,

                (err) => {
                    if (err) {
                        // Table already created
                        logger.info('Suggestions table already exists');
                    }
                });
            logger.info('Building acronyms table');
            alusDatabase.run(`CREATE TABLE Acronyms (
                            acronym_id INTEGER PRIMARY KEY AUTOINCREMENT,
                            acronym TEXT NOT NULL,
                            definition TEXT NOT NULL,
                            description TEXT NOT NULL )`,
                (err) => {
                    if (err) {
                        // Table already created
                        logger.info('Acronyms table already exists');
                    } else{
                        logger.info('Adding default acronyms to acronym list');
                        // Table just created, creating some rows
                        const insert = 'INSERT INTO Acronyms (acronym, definition, description) VALUES (?,?,?)'
                        alusDatabase.run(insert, ["DWP", "Department for Work and Pensions", "Responsible for welfare, pensions and child maintenance policy. As the UK’s biggest public service department it administers the State Pension and a range of working age, disability and ill health benefits to around 20 million claimants and customers."])
                        alusDatabase.run(insert, ["BACS", "Bankers' Automated Clearing System", "Responsible for the schemes behind the clearing and settlement of UK automated payment methods, Direct Debit and Bacs Direct Credit."])
                        alusDatabase.run(insert, ["DLA", "Disabled Living Allowance", "A monthly payment to help with care and mobility needs if you are living with a disability."])
                        alusDatabase.run(insert, ["JSA", "Job Seekers' Allowance", "An unemployment benefit paid by the Government of the United Kingdom to people who are unemployed and actively seeking work."])
                        alusDatabase.run(insert, ["NINO", "National Insurance Number", "Your National Insurance number is your own personal account number. The number makes sure that the National Insurance contributions and tax you pay are properly recorded against your name."])
                    }
                });
            logger.info('Building approval table');
            alusDatabase.run(`CREATE TABLE Approval (
                            approval_id INTEGER PRIMARY KEY AUTOINCREMENT,
                            suggestion_id INTEGER NOT NULL, 
                            admin_id INTEGER NOT NULL, 
                            approved_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
                            FOREIGN KEY (suggestion_id) REFERENCES Suggestions (suggestion_id), 
                            FOREIGN KEY (admin_id) REFERENCES Users (user_id) )`,

                (err) => {
                    if (err) {
                        // Table already created
                        logger.info('Approvals table already exists')
                    }
                });
            alusDatabase.close();
        }
    });
}

// A function that retrieves all the acronyms and sends them to the home page
const getAcronyms = (req, res) => {
    // Creating a new database connection
    const alusDatabase = new sqlite3.Database(databaseSource);
    // Querying the Acronyms table
    alusDatabase.all('SELECT * FROM Acronyms ORDER BY acronym ASC', (err, rows) => {
        if (err) {
            logger.error({ message: 'Error occurred when attempting to collect acronyms from DB', reason: err.message });
            res.render('error-page');
            return;
        }
        alusDatabase.close()

        if (req.session) {
            res.render('home', { username: req.session.username, is_admin: req.session.isAdmin,  acronyms: rows });
        } else {
            res.render('home', { acronyms: rows });
        }
    });
};

module.exports = {
    createDatabase,
    getAcronyms,
};
