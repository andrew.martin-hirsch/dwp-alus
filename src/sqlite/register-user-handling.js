const bcrypt = require("bcryptjs");
const logger = require("../controller/utils/logger");
const joi = require("joi");
const sqlite3 = require('sqlite3').verbose()
const databaseSource = "alus.db"
require("dotenv").config();

// A function that validates each component of the post request
const validatePostBody = (body) => {
    const schema = joi.object({
        email: joi.string().email().required(),
        username: joi.string().required(),
        password: joi.string().required(),
        confirm_password: joi.string().required(),
        csrf: joi.string(),
    });
    return schema.validate(body, {
        abortEarly: false,
    });
};

// A function that handles a post request to register a new user
const postRegisterRequest = async (req, res) => {
    try {

        const validateFilter = validatePostBody(req.body);
        if (validateFilter.error) {
            const { message } = validateFilter.error;
            logger.info(`Invalid post body ${message}`);
            return res.status(400).send(message);
        }

        // Extracting the username, email and password from the request body
        const {email, username, password, confirm_password} = req.body;

        if (password !== confirm_password) {
            req.session.passworderror = "The passwords you entered did not match.";
            return res.redirect("/register");
        }

        // Creating a new database connection
        const alusDatabase = new sqlite3.Database(databaseSource);

        // Selecting a user with the provided email
        const sql = "SELECT * FROM Users WHERE email = ?";
        const existingUser = await new Promise((resolve, reject) => {
            alusDatabase.get(sql, [email], (err, row) => {
                if (err) reject(err);
                resolve(row);
            });
        });

        // If a user with the provided email already exists, close the database connection and return an error response
        if (existingUser) {
            alusDatabase.close();
            req.session.err = "A user with this email already exists. Please login.";
            return res.redirect("/register");
        }

        // Generating a salt for the password
        const salt = bcrypt.genSaltSync(10);
        // Hashing the password with the generated salt
        const hashedpassword = bcrypt.hashSync(password, salt);
        // Getting the current date and time
        const dateCreated = Date.now();

        // Inserting the new user into the database
        const insertSql = 'INSERT INTO Users (username, email, password, salt, date_created) VALUES (?,?,?,?,?)';
        const insertParams = [username, email, hashedpassword, salt, dateCreated];

        alusDatabase.run(insertSql, insertParams, function (err) {
            alusDatabase.close();

            // If there's an error inserting the new user, return an error response
            if (err) {
                return res.status(400).json({ error: err.message });
            }

            // If the user was inserted successfully, return a success response
            return res.render('register_success.njk');
        });
    } catch (error) {
        // If there's an error during the execution of the function, log the error and return a server error response
        logger.error({ message: 'Error occurred when attempting to register a new user', reason: error.message });
        return res.status(500).json({ error: "Server error." });
    }
};

module.exports = {
    postRegisterRequest,
};
