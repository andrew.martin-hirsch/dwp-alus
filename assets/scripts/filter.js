function filterList() {
    let input, filter, dl, rows, dt, dd, i, txtValue;
    input = document.getElementById("user-input");
    filter = input.value.toUpperCase();
    dl = document.getElementById("acronym-list");
    rows = dl.getElementsByClassName("govuk-summary-list__row");

    for (i = 0; i < rows.length; i++) {
        dt = rows[i].getElementsByTagName("dt")[0];
        dd = rows[i].getElementsByTagName("dd")[0];
        if (dt || dd) {
            txtValue = (dt.textContent || dt.innerText) + ' ' + (dd.textContent || dd.innerText);
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                rows[i].style.display = "";
            } else {
                rows[i].style.display = "none";
            }
        }
    }
}
const input = document.getElementById("user-input");
input.addEventListener('keyup', filterList)
