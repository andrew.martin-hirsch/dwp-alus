/* eslint-disable no-unused-expressions, no-underscore-dangle, no-shadow */
const rewire = require('rewire');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const { expect } = chai;
chai.use(sinonChai);
const indexRoute = rewire('../../../src/route/manage-routes');

describe('routes', () => {
    describe('index', () => {
        const fakes = {
            get: [
                { endpoint: '/acronyms', funName: 'getAcronyms', middleware: 'setCSRFToken' },
                { endpoint: '/home', funName: 'getAcronyms', middleware: 'setCSRFToken' },
                { endpoint: '/add-suggestion', funName: 'getAddSuggestionScreen', middleware: 'setCSRFToken' },
                { endpoint: '/register', funName: 'getRegisterScreen', middleware: 'setCSRFToken' },
                { endpoint: '/login', funName: 'getLoginScreen', middleware: 'setCSRFToken' },
                { endpoint: '/view-suggestions', funName: 'getSuggestions', middleware: 'setCSRFToken' },
            ],
            post: [
                { endpoint: '/api/register', funName: 'postRegisterRequest', middleware: 'compareCSRFToken' },
                { endpoint: '/api/login', funName: 'postLoginRequest', middleware: 'compareCSRFToken' },
                { endpoint: '/api/suggestion', funName: 'postSuggestion', middleware: 'compareCSRFToken' },
                { endpoint: '/add-suggestion', funName: 'addSuggestion', middleware: 'compareCSRFToken' },
                { endpoint: '/delete-suggestion', funName: 'deleteSuggestion', middleware: 'compareCSRFToken' },
                { endpoint: '/edit-suggestion', funName: 'getEditSuggestionScreen', middleware: 'setCSRFToken' },
                { endpoint: '/submit-edit', funName: 'editSuggestion', middleware: 'compareCSRFToken' },
                { endpoint: '/logout', funName: 'logout' },
            ],
        };
        const fakeApp = { get: sinon.stub(), post: sinon.stub() };
        const stubRouter = sinon.stub().returns(fakeApp);
        const fakeExpress = { Router: stubRouter };
        indexRoute.__set__('express', fakeExpress);
        indexRoute.__set__('compareCSRFToken', 'compareCSRFToken');
        indexRoute.__set__('setCSRFToken', 'setCSRFToken');
        Object.keys(fakes).forEach((type) => {
            fakes[type].forEach((stub) => {
                indexRoute.__set__(stub.funName, stub.funName);
            });
        });
        let result;
        beforeEach(() => {
            stubRouter.resetHistory();
            fakeApp.get.resetHistory();
            fakeApp.post.resetHistory();
            result = indexRoute.getRoutes();
        });
        it('should return the app', () => {
            expect(result).to.eql(fakeApp);
        });
        it('should call app.get', () => {
            expect(fakeApp.get).have.callCount(fakes.get.length);
        });
        fakes.get.forEach(({ endpoint, funName, middleware }, index) => {
            it(`should assign GET route ${index} as ${endpoint}`, () => {
                if (middleware) {
                    expect(fakeApp.get.getCall(index))
                        .to.have.been.calledWith(endpoint, middleware, funName);
                } else {
                    expect(fakeApp.get.getCall(index)).to.have.been.calledWith(endpoint, funName);
                }
            });
        });
        it('should call app post', () => {
            expect(fakeApp.post).have.callCount(fakes.post.length);
        });
        fakes.post.forEach(({ endpoint, funName, middleware }, index) => {
            it(`should assign POST route ${index} as ${endpoint}`, () => {
                if (middleware) {
                    expect(fakeApp.post.getCall(index))
                        .to.have.been.calledWith(endpoint, middleware, funName);
                } else {
                    expect(fakeApp.post.getCall(index)).to.have.been.calledWith(endpoint, funName);
                }
            });
        });
    });
});
