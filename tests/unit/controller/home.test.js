const chai = require('chai');
const sinon = require('sinon');
const rewire = require('rewire');
const sinonChai = require('sinon-chai');

const { expect } = chai;
chai.use(sinonChai);

const controllerModule = rewire('../../../src/controller/home');

describe('getHome function', () => {
    const fakeFileLogger = {
        info: sinon.stub(),
        error: sinon.stub(),
    };
    const fakeReq = {};
    const fakeRes = {
        render: sinon.stub(),
    };

    controllerModule.__set__('logger', fakeFileLogger);

    beforeEach(() => {
        fakeFileLogger.info.reset();
        fakeFileLogger.error.resetHistory();
        fakeRes.render.resetHistory();
    });

    it('should render the add-suggestion template', async () => {
        await controllerModule.getHome(fakeReq, fakeRes);
        expect(fakeRes.render).to.be.calledOnceWithExactly('home');
    });

    it('render error page and should log an error', async () => {
        const error = new Error('Fake ALUS error');
        fakeFileLogger.info.onFirstCall().throws(error);
        await controllerModule.getHome(fakeReq, fakeRes);
        expect(fakeRes.render).to.be.calledOnceWithExactly('error-page');
        expect(fakeFileLogger.info.args[0][0]).to.eql('Rendering home page');
        expect(fakeFileLogger.error.args[0][0].reason).to.eql('Fake ALUS error');
        expect(fakeFileLogger.error.args[0][0].message).to.eql('Error occurred rendering home page');
    });
});
