const chai = require('chai');
const sinon = require('sinon');
const rewire = require('rewire');
const sinonChai = require('sinon-chai');

const { expect } = chai;
chai.use(sinonChai);

const controllerModule = rewire('../../../src/controller/get-login-screen');

describe('getLoginScreen function', () => {
    const fakeFileLogger = {
        info: sinon.stub(),
        error: sinon.stub(),
    };
    const fakeReq = { session: { csrf: 'csrf', err: 'err' }};
    const fakeRes = {
        render: sinon.stub(),
    };

    const fakeData = {
        csrf: fakeReq.session.csrf,
        err: fakeReq.session.err,
    }

    controllerModule.__set__('logger', fakeFileLogger);

    beforeEach(() => {
        fakeFileLogger.info.reset();
        fakeFileLogger.error.resetHistory();
        fakeRes.render.resetHistory();
    });

    it('should render the login page with supplied data', async () => {
        await controllerModule.getLoginScreen(fakeReq, fakeRes);
        expect(fakeRes.render).to.be.calledOnceWithExactly('login', fakeData);
    });

    it('render error page and should log an error', async () => {
        const error = new Error('Fake ALUS error');
        fakeFileLogger.info.onFirstCall().throws(error);
        await controllerModule.getLoginScreen(fakeReq, fakeRes);
        expect(fakeRes.render).to.be.calledOnceWithExactly('error-page');
        expect(fakeFileLogger.info.args[0][0]).to.eql('Rendering login page');
        expect(fakeFileLogger.error.args[0][0].reason).to.eql('Fake ALUS error');
        expect(fakeFileLogger.error.args[0][0].message).to.eql('Error occurred rendering login page');
    });
});
