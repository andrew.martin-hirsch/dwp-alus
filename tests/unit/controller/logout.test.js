const chai = require('chai');
const sinon = require('sinon');
const rewire = require('rewire');
const sinonChai = require('sinon-chai');
const app = require('express/lib/router');
const session = require('express-session');
const { expect } = chai;
chai.use(sinonChai);
const logoutModule = rewire('../../../src/controller/logout');
describe('logout function', () => {
    let req, res, getAcronymsStub;
    const fakeFileLogger = {
        info: sinon.stub(),
        error: sinon.stub(),
    };
    const fakeRes = {
        render: sinon.stub(),
    };
    logoutModule.__set__('logger', fakeFileLogger);
    beforeEach(() => {
        req = {
            session: {
                destroy: () => {}
            }
        };
        res = {
            send: () => {}
        };
        getAcronymsStub = sinon.stub();
        logoutModule.__set__('getAcronyms', getAcronymsStub);
        fakeFileLogger.info.reset();
        fakeFileLogger.error.resetHistory();
    });
    afterEach(() => {
        sinon.restore();
    });
    it('should destroy session', () => {
        const destroyStub = sinon.stub(req.session, 'destroy').yields();
        logoutModule.logout(req, res);
        expect(destroyStub.calledOnce).to.be.true;
    });
    it('should log session destroyed message to console and call getAcronyms', () => {
        const destroyStub = sinon.stub(req.session, 'destroy').yields();
        logoutModule.logout(req, res);
        expect(fakeFileLogger.info.args[0][0]).to.eql('User logged out, session is destroyed');
        expect(getAcronymsStub).to.be.calledOnce;
    });
    it('should handle session destroy error', () => {
        const error = new Error('Fake session destroy error');
        const destroyStub = sinon.stub(req.session, 'destroy').yields(error);
        logoutModule.logout(req, fakeRes);
        expect(fakeFileLogger.error.args[0][0].reason).to.eql('Fake session destroy error');
        expect(fakeFileLogger.error.args[0][0].message).to.eql('Error occurred when attempting to destroy session');
    });
    it('should log session destroyed message and call getAcronyms when session is destroyed successfully', () => {
        const destroyStub = sinon.stub(req.session, 'destroy').yields(null); // No error
        logoutModule.logout(req, res);
        expect(fakeFileLogger.info.args[0][0]).to.eql('User logged out, session is destroyed');
        expect(getAcronymsStub).to.be.calledOnce;
    });
});
