const chai = require('chai');
const sinon = require('sinon');
const rewire = require('rewire');
const sinonChai = require('sinon-chai');

const { expect } = chai;
chai.use(sinonChai);

const controllerModule = rewire('../../../src/controller/get-register-screen');

describe('getRegisterScreen function', () => {
    const fakeFileLogger = {
        info: sinon.stub(),
        error: sinon.stub(),
    };
    const fakeReq = { session: { csrf: 'csrf', err: 'fakeerror', passworderror: 'fakePWerror' }};
    const fakeRes = {
        render: sinon.stub(),
    };
    const fakeData = {
        csrf: fakeReq.session.csrf,
        user_exists_error: fakeReq.session.err,
        password_match_error: fakeReq.session.passworderror,
    }

    controllerModule.__set__('logger', fakeFileLogger);

    beforeEach(() => {
        fakeFileLogger.info.reset();
        fakeFileLogger.error.resetHistory();
        fakeRes.render.resetHistory();
    });

    it('should render the register page with supplied data', async () => {
        await controllerModule.getRegisterScreen(fakeReq, fakeRes);
        expect(fakeRes.render).to.be.calledOnceWithExactly('register', fakeData);
    });

    it('render error page and should log an error', async () => {
        const error = new Error('Fake ALUS error');
        fakeFileLogger.info.onFirstCall().throws(error);
        await controllerModule.getRegisterScreen(fakeReq, fakeRes);
        expect(fakeRes.render).to.be.calledOnceWithExactly('error-page');
        expect(fakeFileLogger.info.args[0][0]).to.eql('Rendering register page');
        expect(fakeFileLogger.error.args[0][0].reason).to.eql('Fake ALUS error');
        expect(fakeFileLogger.error.args[0][0].message).to.eql('Error occurred rendering register page');
    });
});
