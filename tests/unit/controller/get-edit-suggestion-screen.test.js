const chai = require('chai');
const sinon = require('sinon');
const rewire = require('rewire');
const sinonChai = require('sinon-chai');

const { expect } = chai;
chai.use(sinonChai);

const controllerModule = rewire('../../../src/controller/get-edit-suggestion-screen');

describe('getEditSuggestionScreen function', () => {
    const fakeFileLogger = {
        info: sinon.stub(),
        error: sinon.stub(),
    };
    const fakeReq = {body: {suggestion_id: 'fake-id'},
                    session: {csrf: 'csrf'}};
    const fakeRes = {
        render: sinon.stub(),
    };
    const fakeData = {
        suggestion_id: 'fake-id',
        csrf: fakeReq.session.csrf,
    }
    controllerModule.__set__('logger', fakeFileLogger);

    beforeEach(() => {
        fakeFileLogger.info.reset();
        fakeFileLogger.error.resetHistory();
        fakeRes.render.resetHistory();
    });

    it('should render the add-suggestion template', async () => {
        await controllerModule.getEditSuggestionScreen(fakeReq, fakeRes);
        expect(fakeRes.render).to.be.calledOnceWithExactly('edit-suggestion', fakeData);
    });

    it('render error page and should log an error', async () => {
        const error = new Error('Fake ALUS error');
        fakeFileLogger.info.onFirstCall().throws(error);
        await controllerModule.getEditSuggestionScreen(fakeReq, fakeRes);
        expect(fakeRes.render).to.be.calledOnceWithExactly('error-page');
        expect(fakeFileLogger.info.args[0][0]).to.eql('Rendering edit suggestion page');
        expect(fakeFileLogger.error.args[0][0].reason).to.eql('Fake ALUS error');
        expect(fakeFileLogger.error.args[0][0].message).to.eql('Error occurred rendering Edit Suggestion page');
    });
});
