const chai = require('chai');

const CSRFAttackError = require('../../../src/middleware/CSRFAttackError');

const { expect } = chai;

describe('CSRFAttackError', () => {
    const err = new CSRFAttackError();
    expect(err.name).to.eql('CSRFAttackError');
    expect(err.message).to.eql('Detected a Cross-Site Request Forgery Attempt');
});
