/* eslint-disable no-underscore-dangle */
const chai = require('chai');
const sinon = require('sinon');
const rewire = require('rewire');
const sinonChai = require('sinon-chai');

const { expect } = chai;
chai.use(sinonChai);
const filePath = '../../../src/middleware/csrf.js';

describe('csrf middleware functions', () => {
    const stubNext = sinon.stub();
    const stubUUID = sinon.stub().returns('test');

    const subject = rewire(filePath);
    subject.__set__('uuidv4', stubUUID);
    let fakeReq;

    beforeEach(() => {
        fakeReq = {
            session: {},
            originalUrl: 'aUrl',
            body: {},
        };
        stubNext.resetHistory();
    });

    describe('setCSRFToken', () => {
        it('sets a token onto session key: csrf', () => {
            subject.setCSRFToken(fakeReq, {}, stubNext);
            expect(fakeReq.session.csrf).to.eql('test');
            expect(stubUUID).callCount(1);
            expect(stubNext).callCount(1);
        });
    });

    describe('compareCSRFToken', () => {
        it('csrf tokens match, calls next', () => {
            fakeReq.session.csrf = 'test';
            fakeReq.body = { csrf: 'test' };
            subject.compareCSRFToken(fakeReq, {}, stubNext);
            expect(fakeReq.session.csrf).to.eql(undefined);
            expect(stubNext).callCount(1);
        });

        it('csrf tokens do not match, throws an error', () => {
            try {
                fakeReq.session.csrf = 'test';
                fakeReq.body = { csrf: 'token' };
                subject.compareCSRFToken(fakeReq, {}, stubNext);
            } catch (error) {
                expect(error.message).to.eql('Detected a Cross-Site Request Forgery Attempt');
                expect(stubNext).callCount(0);
            }
        });

        it('does not have csrfAct token, throws an error', () => {
            try {
                fakeReq.body = { csrf: 'token' };
                subject.compareCSRFToken(fakeReq, {}, stubNext);
            } catch (error) {
                expect(error.message).to.eql('Detected a Cross-Site Request Forgery Attempt');
                expect(stubNext).callCount(0);
            }
        });

        it('does not have csrf tokens', () => {
            try {
                fakeReq.body = {};
                subject.compareCSRFToken(fakeReq, {}, stubNext);
            } catch (error) {
                expect(error.message).to.eql('Detected a Cross-Site Request Forgery Attempt');
                expect(stubNext).callCount(0);
            }
        });

        it('skips csrf when checked flag is true', () => {
            fakeReq.session = { csrfChecked: true };
            subject.compareCSRFToken(fakeReq, {}, stubNext);
            expect(stubNext).callCount(1);
            expect(fakeReq.session.csrfChecked).to.eq(undefined);
        });
    });
});
